<?php
function connect(){
    $dbhost = "localhost";
    $dbuser = "root";
    $dbpass = "";
    $dbname = "organisasi";
    return mysqli_connect($dbhost,$dbuser,$dbpass,$dbname);
}

function getTableUser(){
    $link = connect();
    $query = "SELECT *  FROM user";
    $result = mysqli_query($link, $query);

    $hasil = mysqli_fetch_all($result, MYSQLI_ASSOC);
    return $hasil;
}

function getName() {
    $link = connect();
    $query = "SELECT nama  FROM user";
    $result = mysqli_query($link, $query);
    return $result;
}

function logout() {
    session_start();
    if(isset($_SESSION['username'])){
        unset($_SESSION);
        session_destroy();
    }
    header("location:/modul03/index.php");
}

?>