<?php
session_start();
isset($_SESSION['login'])?$session=$_SESSION['login']:$session="";
if($session==false){
    include "view/view_login.php";
}else{
    isset($_GET['page'])?$page=$_GET['page']:$page="";
    if($page==""){
        include "view/view_dashboard.php";
    }else if($page=="anggota"){
        include "controller/controller_anggota.php";
    }else if($page=="pengurus"){
        include "controller/controller_pengurus.php";
    }else if($page=="pendaftaran"){
        include "view/view_register.php";
    }else if($page=="logout"){
        include 'model/model_user.php';
        logout();
    }else{
        echo "Not found";
    }
}
?>