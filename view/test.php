 <!DOCTYPE html>
 <html lang="en">

 <head>
     <meta charset="UTF-8">
     <meta name="viewport" content="width=device-width, initial-scale=1.0">
     <meta http-equiv="X-UA-Compatible" content="ie=edge">
     <title>Testimonial</title>
     <link href="assets/css/dashboard.css" rel="stylesheet">
     <link rel="stylesheet" href="assets/css/testimonial.css">
     <link rel="stylesheet" href="OwlCarousel2-2.3.4/dist/assets/owl.carousel.css">
     <link rel="stylesheet" href="OwlCarousel2-2.3.4/dist/assets/owl.theme.default.min.css">
     <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BmbxuPwQa2lc/FVzBcNJ7UAyJxM6wuqIj61tLrc4wSX0szH/Ev+nYRRuWlolflfl" crossorigin="anonymous">
     <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
     <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
     <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
     <link href="https://fonts.googleapis.com/css?family=Francois+One&display=swap" rel="stylesheet">
     <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css" integrity="sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ" crossorigin="anonymous">
 </head>

 <body>
     <?= include "view/header.php" ?>
     <section class="testimonial">
         <div class="row">
             <div class="col-12" style="margin-top:-20px">
                 <p class="title">Pengurus Periode 2020/2021</p>
             </div>
         </div>
         <div class="owl-carousel owl-theme">
             <?php
             
                foreach ($tablePengurus as $data) {
                    echo "<div class='item'>";
                    echo "<div class='card-deck'>";
                    echo " <div class='card'>";
                    echo " <div class='card-body'>";
                    echo " <h4 class='card-title'>$data[nama_pengurus]</h4>";
                    echo "<p class='card-text' id='desc'>$data[jabatan]</p> ";
                    echo "<p class='card-text' id='desc'>$data[testimoni]</p> ";
                    echo " </div";
                    echo "</div>";
                    echo "</div>";
                    echo "</div>";
                }
                ?>
         </div>
     </section>



     <script src="OwlCarousel2-2.3.4/docs/assets/vendors/jquery.min.js"></script>
     <script src="OwlCarousel2-2.3.4/dist/owl.carousel.js"></script>
     <script>
         $('.owl-carousel').owlCarousel({
             loop: true,
             margin: 10,
             nav: true,
             responsive: {
                 0: {
                     items: 1
                 },
                 600: {
                     items: 3
                 },
                 1000: {
                     items: 5
                 }
             }
         })
     </script>

 </body>

 </html>
 <script>
     $(document).ready(function() {

         $("#owl-demo").owlCarousel({

             autoPlay: 3000, //Set AutoPlay to 3 seconds

             items: 4,
             itemsDesktop: [1199, 3],
             itemsDesktopSmall: [979, 3]
         });
     });
 </script>
 <script src="assets/js/dashboard.js"></script>