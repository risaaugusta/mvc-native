<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Anggota</title>
    <link href="assets/css/dashboard.css" rel="stylesheet" >
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/css/bootstrap.min.css" rel="stylesheet" 
    integrity="sha384-BmbxuPwQa2lc/FVzBcNJ7UAyJxM6wuqIj61tLrc4wSX0szH/Ev+nYRRuWlolflfl" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.6.0/dist/umd/popper.min.js" integrity="sha384-KsvD1yqQ1/1+IA7gi3P0tyJcT3vR+NdBTt13hSJ2lnve8agRGXTTyNaBYmCR/Nwi" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/js/bootstrap.min.js" integrity="sha384-nsg8ua9HAw1y0W1btsyWgBklPnCUAFLuTMS2G72MMONqmOymq585AcH49TLBQObG" crossorigin="anonymous"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="assets/js/dashboard.js"></script>
</head>
<body>
<?= include "view/header.php";
// include_once 'model/model_anggota.php';
?>
<div class="container">
<table class="table">
  <thead class="table-light">
    <th>No</th>
    <th>Nama</th>
    <th>Jurusan</th>
    <th>Divisi</th>
    <th>Tahun masuk</th>
    <!-- <th>Aksi</th> -->
  </thead>
  <tbody>
    <?php
                foreach ($tableAnggota as $data) {
                    echo "<tr>";
                    echo "<td>$data[id_anggota]</td>";
                    echo "<td>$data[nama_anggota]</td>";
                    echo "<td>$data[jurusan]</td>";
                    echo "<td>$data[divisi]</td>";
                    echo "<td>$data[tahun]</td>";
                    echo "</tr>";
                  }
                  
     ?>
  </tbody>
</table>
</div>   
</div>
</body>
</html>