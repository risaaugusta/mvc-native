<nav class="navbar navbar-expand-lg navbar-mainbg">
    <a class="navbar-brand navbar-logo" href="#">Metic</a>
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav ml-auto">
            <div class="hori-selector">
                <div class="left"></div>
                <div class="right"></div>
            </div>
            <li class="nav-item active">
                <a class="nav-link" href="javascript:void(0);">Dashboard</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="?page=anggota">Data Anggota</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="?page=pengurus">Pengurus</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="?page=pendaftaran">Pendaftaran</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="?page=logout" onclick="return confirm('Anda yakin akan keluar?')">Logout</a>
            </li>
        </ul>
    </div>
</nav>
</nav>
