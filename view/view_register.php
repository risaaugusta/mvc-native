<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link href="assets/css/dashboard.css" rel="stylesheet" >
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/css/bootstrap.min.css" rel="stylesheet" 
    integrity="sha384-BmbxuPwQa2lc/FVzBcNJ7UAyJxM6wuqIj61tLrc4wSX0szH/Ev+nYRRuWlolflfl" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.6.0/dist/umd/popper.min.js" integrity="sha384-KsvD1yqQ1/1+IA7gi3P0tyJcT3vR+NdBTt13hSJ2lnve8agRGXTTyNaBYmCR/Nwi" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/js/bootstrap.min.js" integrity="sha384-nsg8ua9HAw1y0W1btsyWgBklPnCUAFLuTMS2G72MMONqmOymq585AcH49TLBQObG" crossorigin="anonymous"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="assets/js/dashboard.js"></script>
    <style>
         form{
             /* margin-top: 20px; */
             margin-left: 200px;
             margin-right:200px;
         }
     </style>
</head>
<body>
<?=  include 'view/header.php';
include_once 'model/model_anggota.php';?>
    <div class="container mt-5">
    <form name="myForm" id="myForm" action="<?= insertAnggota()?>" method="post">
        <div class="mb-3">
            <input type="text" class="form-control" name="nama_anggota" id="nama_anggota" placeholder="Nama lengkap" required >
          </div>
          <div class=" mb-3">
            <input type="text" class="form-control" name="jurusan" id="jurusan" placeholder="Jurusan" required >
          </div>
        
            <div class="mb-3"><br>
                <label for="" >Divisi</label>
                <select class="form-select  " name="divisi" id ="divisi"aria-label="Default select example" required>
                    <option selected>-- Pilih --</option>
                    <option value="Programming">Programming</option>
                    <option value="Networking">Networking</option>
                    <option value="Design">Design</option>
                    <option value="Robotic">Robotic</option>
                  </select>
            </div>
            <div class="  mb-3" >
                <input type="number" class="form-control" name="tahun" id="tahun" placeholder="Tahun masuk" required >
            </div>
        <div class="d-grid gap-2 d-md-block">
            <button class="btn btn-primary" type="submit" name="kirim">Daftarkan</button>
            <button class="btn btn-light" type="button">Hapus</button>
          </div>
    </form>
    </div>
</body>
</html>

 
 

